This is how you run this thing
------------------------------

Contact yihungjen@macrodatalab.com for access to BigObject docker image.

tl;dr
=====

```
docker run -it --rm this_image_name
```

execute **benchmark**

To switch the benchmark profile, edit benchmark using *vim*

What you don't know VIM?  Well, how are you viewing this document?

more info
=========

Copy the *setup* script from container under */benchmark*

Benchmark cases are written in Python, so the script itself should be
self-explanatory.

Uncomment sections to enable/disable benchmark case, but *sqlite3* being
default since there are setup required

To run benchmark against *BigObject* and/or *Redis*, setup containers by
running *setup* script from source or run following script.

```
#!/bin/bash

docker rm -f volume bosrvd redis

docker create --name volume \
    dev.bigobject.io/macrodata/bosrvd_volume

docker run -t -d --name bosrvd \
    --volumes-from volume \
    dev.bigobject.io/macrodata/bosrvd

docker run -t -d --name redis \
    redis
```

Create link to Redis and BigObject by

```
docker run -it --rm \
    --link redis:redis --link bosrvd:bosrvd \
    this_image_name
```


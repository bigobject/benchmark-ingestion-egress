KEY_RNG = 10000
PAYLOAD_SIZE = 100
REQUEST_NUMBER = 10000

# generate key set from range
keyset = map(lambda x: 'keys:' + str(x), xrange(KEY_RNG))

from data import gen_data

##########################################################################################

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import class_mapper, Query, sessionmaker

import random

_engine = create_engine('sqlite:///:memory:', echo=False)

Session = sessionmaker(bind=_engine)
Base = declarative_base(bind=_engine)

class _QueryProperty(object):
    def __get__(self, instance, owner):
        mapper = class_mapper(owner)
        return Query(mapper, session=Session())

class Hash(Base):
    __tablename__ = 'Hash'

    key = Column(String, primary_key=True, index=True)
    value = Column(String)

    query = _QueryProperty()

    def __init__(self, key, value):
        self.key = key
        self.value = value

def setup():
    Base.metadata.drop_all()
    Base.metadata.create_all()

def set_benchmark():
    for key in keyset:
        # generate data to set
        data = gen_data(PAYLOAD_SIZE)
        session = Session()
        session.add(Hash(key, data))
        session.commit()

def set_pipeline():
    session = Session()
    for key in keyset:
        # generate data to set
        data = gen_data(PAYLOAD_SIZE)
        session.add(Hash(key, data))
    else:
        session.commit()

def get_benchmark():
    for _ in xrange(REQUEST_NUMBER):
        Hash.query.filter_by(key=random.choice(keyset)).first()

def get_pipeline():
    Hash.query.all()

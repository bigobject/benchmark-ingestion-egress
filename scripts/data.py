import os
import binascii

def gen_data(size):
    # generate data to set
    return binascii.b2a_hex(os.urandom(size))


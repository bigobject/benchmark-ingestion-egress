KEY_RNG = 100
PAYLOAD_SIZE = 100
REQUEST_NUMBER = 10000

# generate key set from range
keyset = map(lambda x: 'keys:' + str(x), xrange(KEY_RNG))

from data import gen_data

##########################################################################################

import os
import redis
import random

REDIS_ADDR = os.environ['REDIS_PORT_6379_TCP_ADDR']
REDIS_PORT = int(os.environ['REDIS_PORT_6379_TCP_PORT'])
REDIS_INST = None

def setup():
    global REDIS_INST
    pool = redis.ConnectionPool(
        host=REDIS_ADDR,
        port=REDIS_PORT,
        db=0,
        socket_keepalive=True
    )
    REDIS_INST = redis.StrictRedis(connection_pool=pool)

def set_benchmark():
    for _ in xrange(REQUEST_NUMBER):
        # generate data to set
        data = gen_data(PAYLOAD_SIZE)
        REDIS_INST.set(random.choice(keyset), data)

def set_pipeline():
    p = REDIS_INST.pipeline()
    for _ in xrange(REQUEST_NUMBER):
        # generate data to set
        data = gen_data(PAYLOAD_SIZE)
        p.set(random.choice(keyset), data)
    else:
        p.execute()

def get_benchmark():
    for _ in xrange(REQUEST_NUMBER):
        REDIS_INST.get(random.choice(keyset))

def get_pipeline():
    p = REDIS_INST.pipeline()
    for _ in xrange(REQUEST_NUMBER):
        p.get(random.choice(keyset))
    else:
        p.execute()

KEY_RNG = 100
PAYLOAD_SIZE = 100
REQUEST_NUMBER = 10000

# generate key set from range
keyset = map(lambda x: 'keys:' + str(x), xrange(KEY_RNG))

from data import gen_data

##########################################################################################

from bosrv.request import connect
from bosrv.ttypes import *

import os
import random
import urlparse
import functools

BIGOBJECT_ADDR = os.environ['BOSRVD_PORT_9090_TCP_ADDR']
BIGOBJECT_PORT = int(os.environ['BOSRVD_PORT_9090_TCP_PORT'])

BIGOBJECT_URL = urlparse.urlunsplit(('bo', '%s:%d' % (BIGOBJECT_ADDR, BIGOBJECT_PORT), '', '', ''))

def setup():
    with connect(BIGOBJECT_URL, timeout=None) as conn:
        token, cli = conn
        execute = functools.partial(cli.execute, token='', workspace='', opts='')
        try:
            execute(stmt='DROP TABLE hash')
        except:
            pass
        finally:
            execute(stmt='CREATE TABLE hash (id STRING, value VARSTRING, KEY(id))')

def set_benchmark():
    with connect(BIGOBJECT_URL, timeout=None) as conn:
        token, cli = conn
        execute = functools.partial(cli.execute, token='', workspace='', opts='')
        for _ in xrange(REQUEST_NUMBER):
            # generate data to set
            data = gen_data(PAYLOAD_SIZE)
            execute(stmt='INSERT INTO hash VALUES (%s, %s)' % (random.choice(keyset), data))

def set_pipeline():
    with connect(BIGOBJECT_URL, timeout=None) as conn:
        token, cli = conn
        execute = functools.partial(cli.execute, token='', workspace='', opts='')
        insert_stmt = 'INSERT INTO hash VALUES ' + ' '.join(map(
            lambda x: '(%s, %s)' % (random.choice(keyset), gen_data(PAYLOAD_SIZE)),
            xrange(REQUEST_NUMBER)
        ))
        execute(stmt=insert_stmt)

def get_benchmark():
    with connect(BIGOBJECT_URL, timeout=None) as conn:
        token, cli = conn
        execute = functools.partial(cli.execute, token='', workspace='', opts='')
        cursor_fetch = functools.partial(cli.cursor_fetch, token='', rng=RangeSpec(page=KEY_RNG))
        cursor_close = functools.partial(cli.cursor_close, token='')
        for _ in xrange(REQUEST_NUMBER):
            table = execute(stmt='select * from hash where id=%s' % random.choice(keyset))
            cursor_fetch(resource=table)
            cursor_close(resource=table)

def get_pipeline():
    with connect(BIGOBJECT_URL, timeout=None) as conn:
        token, cli = conn
        execute = functools.partial(cli.execute, token='', workspace='', opts='')
        cursor_fetch = functools.partial(cli.cursor_fetch, token='', rng=RangeSpec(page=KEY_RNG))
        cursor_close = functools.partial(cli.cursor_close, token='')

        table = execute(stmt='select * from hash')
        cursor_fetch(resource=table)
        cursor_close(resource=table)

FROM ubuntu:latest

# install core dependencies
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    python \
    python-dev \
    curl \
    vim \
    redis-tools \
    sqlite3

# install python package manageer
RUN curl https://bootstrap.pypa.io/get-pip.py | python -

# install app dependency
RUN pip install redis hiredis bosh sqlalchemy ipython

VOLUME /benchmark
WORKDIR /benchmark

CMD /bin/bash

COPY scripts/ /benchmark/scripts/
COPY README.md setup benchmark /benchmark/
